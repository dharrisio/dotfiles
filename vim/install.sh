# Vundle
#
# Installs Vundle and updates vim plugins

DEINBASE=$HOME/.config/nvim
DEININSTALL=$DEINBASE/bundle/repos/github.com/Shougo/dein.vim
VIMRC=$DEINBASE/init.vim

# Check for Dein
if [ ! -d "$DEININSTALL" ]
then
  echo "  Installing Dein for you."
  git clone https://github.com/Shougo/dein.vim.git $DEININSTALL
else
  echo "  Updating Dein for you."
  git -C $DEININSTALL pull
fi

$DEININSTALL/bin/installer.sh $DEINBASE

sudo pip3 install neovim

if [[ ! -L "$VIMRC" ]]
then
	if [[ ! -d "$HOME/.config/nvim" ]]
	then
		mkdir -p $HOME/.config/nvim
	fi
	ln -s $(pwd -P)/vim/vimrc.symlink $VIMRC
fi
