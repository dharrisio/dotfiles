if [[ ! -L "$HOME/.gnupg/gpg.conf" ]]
then
  ln -s $(pwd -P)/gnupg/gpg.conf $HOME/.gnupg/gpg.conf
fi

if [[ ! -L "$HOME/.gnupg/gpg-agent.conf" ]]
then
  ln -s $(pwd -P)/gnupg/gpg-agent.conf $HOME/.gnupg/gpg-agent.conf
fi
