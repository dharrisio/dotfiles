if [ ! -f "${HOME}/.gpg-agent-info" ]; then
	gpg-agent --daemon --enable-ssh-support > $HOME/.gpg-agent-info
fi

. "${HOME}/.gpg-agent-info"
export GPG_AGENT_INFO
export SSH_AUTH_SOCK
export GPG_TTY=$(tty)
