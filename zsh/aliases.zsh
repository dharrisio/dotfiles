alias reload!='. ~/.zshrc'

alias cls='clear' # Good 'ol Clear Screen command

alias gpg=gpg2
