# vim:ft=zsh ts=2 sw=2 sts=2

PROMPT='%{$fg[green]%}%~%{$reset_color%}$(git_prompt_info) %#  '
RPROMPT='$(ve_prompt_info)'

# Must use Powerline font, for \uE0A0 to render.
ZSH_THEME_GIT_PROMPT_PREFIX=" on %{$fg[yellow]%}\uE0A0 "
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg_bold[red]%} !"
ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$fg_bold[green]%} ?"
ZSH_THEME_GIT_PROMPT_CLEAN=""
ZSH_THEME_VE_PROMPT_PREFIX="%{$fg[yellow]%}"
ZSH_THEME_VE_PROMPT_SUFFIX="%{$reset_color%} "
