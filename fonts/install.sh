LINUX_FONT_PATH="$HOME/.local/share/fonts"
MACOS_FONT_PATH="/Library/Fonts"
FONT_DIR="fonts"

if [[ "$(uname -s)" == "Linux" ]]
then
	if [ ! -d $LINUX_FONT_PATH ]
	then
		mkdir -p $LINUX_FONT_PATH
	fi
	cp $FONT_DIR/"Droid Sans Mono for Powerline Nerd Font Complete.otf" $LINUX_FONT_PATH
	fc-cache -f
elif [[ "$(uname -s)" == "Darwin" ]]
then
	cp $FONT_DIR/"Droid Sans Mono for Powerline Nerd Font Complete.otf" $MACOS_FONT_PATH
fi

