export PATH="$PATH:$HOME/.linuxbrew/bin"
export MANPATH="$MANPATH:$HOME/.linuxbrew/share/man"
export INFOPATH="$INFOPATH:$HOME/.linuxbrew/share/info"
export XDG_DATA_DIRS="$XDG_DATA_DIRS:$HOME/.linuxbrew/share"
