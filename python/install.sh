# Install or Upgrade virtualenv from system pip
export PIP_REQUIRE_VIRTUALENV=false
if [[ "$(uname -s)" = "Darwin" ]]
then
  pip install --upgrade pip
  pip install --upgrade virtualenv
else
  sudo -H pip install --upgrade pip
  sudo -H pip install --upgrade virtualenv virtualenvwrapper
fi

# Setup default virtualenv
if [[ ! -d "$HOME/.virtualenvs/default" ]]
then
    mkdir -p $HOME/.virtualenvs
    virtualenv -p python $HOME/.virtualenvs/default
fi

# Update outdated pip packages in default virtualenv
. virtualenvwrapper.sh
workon default
pip install --upgrade pip
pip install --upgrade --requirement python/requirements.txt
