# Outputs current virtual environment
function ve_prompt_info() {
  if [[ -n $VIRTUAL_ENV ]]; then
    ve=$(command basename $VIRTUAL_ENV)
  else
    ve="None"
  fi
  if [[ "$ve" == "default" ]]; then
    ve=""
  fi
  echo "$ZSH_THEME_VE_PROMPT_PREFIX${ve}$ZSH_THEME_VE_PROMPT_SUFFIX"
}
